class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all.reverse
  end

  def create
    @tweet = Tweet.new(tweets_params)
    @tweet.save
    redirect_to root_path
  end

  def delete
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to root_path
  end

  private
  def tweets_params
    params.require(:tweets).permit(:content)
  end
end
